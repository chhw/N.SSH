﻿using DAO.IDAO;
using Domain;
using PX.Core;
using PX.Core.List;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using PX.Code.Core.EntityDAO;
using Domain.IDomain.IDomian;
using NHibernate.Criterion;
using PX.Code.Hibernate;
namespace DAO.Hibernate
{
    public class sys_roleDAO : EntityDAO<sys_role>, Isys_roleDAO
    {
        public IList<string> getRoleUser()
        {
            var a = Session.CreateCriteria().SetProjection(sys_role._.name.ToProperty()).AddOrder(sys_role._.id.ToDesc());
            return a.List<string>();
        }
    }
}

