﻿using DAO.IDAO;
using Domain;
using PX.Core;
using PX.Core.List;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using PX.Code.Core.EntityDAO;
using Domain.IDomain.IDomian;
using NHibernate.Criterion;
namespace DAO.Hibernate
{
    public class sys_userDAO : EntityDAO<sys_user>, Isys_userDAO
    {
        public int getUserCount()
        {
            var proj = Projections.ProjectionList().Add(Projections.RowCount(), "count");
            var a= Session.CreateCriteria();
            a.SetProjection(proj);
            var result = a.UniqueResult<int>();
            return result;
        }
    }
}

