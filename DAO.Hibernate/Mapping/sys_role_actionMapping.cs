﻿using Domain;
using Domain.IDomain.IDomian;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAO.Hibernate.Mapping
{

    public class sys_role_actionMapping : ClassMap<sys_role_action>
    {
       public sys_role_actionMapping()
        {
            Id(i => i.id).GeneratedBy.Native();
            Map(m => m.action_id);
            Map(m => m.role_id);
            Map(m => m.create_time);
            Map(m => m.update_time);

        }
       
    }

 
 }