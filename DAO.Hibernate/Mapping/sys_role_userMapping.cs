﻿using Domain;
using Domain.IDomain.IDomian;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAO.Hibernate.Mapping
{
    public class sys_role_userMapping : ClassMap<sys_role_user>
    {
        public sys_role_userMapping()
        {
            //指定持久化类对应的数据表
            //Table("sys_user");
            //自动增长的id
            Id(i => i.id).GeneratedBy.Native();
            //映射关系
            //Id<Guid>("CustomerID").GeneratedBy.Guid();
            Map(m => m.role_id);
            Map(m => m.user_id);

            Map(m => m.create_time).Default(DateTime.Now.ToString());
            Map(m => m.update_time).Default(DateTime.Now.ToString());

          
        }
    }
}
