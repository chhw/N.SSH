﻿using PX.Code.Base;
using PX.Code.Base.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.IDomain.IDomian
{
    /// <summary>sys_role_action接口</summary>
    /// <remarks></remarks>
    public partial interface Isys_role_action:IEntity
    {
        #region 属性
         /// <summary></summary>
         Int32 id { get; set; }
         /// <summary></summary>
         Int32 action_id { get; set; }
         /// <summary></summary>
         Int32 role_id { get; set; }
         /// <summary></summary>
         DateTime create_time { get; set; }
         /// <summary></summary>
         DateTime update_time { get; set; }
        #endregion
        Isys_role_action_ _X { get; }
     }
      public interface Isys_role_action_
        {
            /// <summary></summary>
            Field id { get;}
            /// <summary></summary>
            Field action_id { get;}
            /// <summary></summary>
            Field role_id { get;}
            /// <summary></summary>
            Field create_time { get;}
            /// <summary></summary>
            Field update_time { get;}
        }
}