﻿using Domain.IDomain;
using Domain.IDomain.IDomian;
using PX.Code.Base;
using PX.Code.Base.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain
{
    [Serializable]
    [EntityAttribute(ConnName="Con",Name="")]
    public partial class sys_role: Entity<sys_role>,Isys_role
    {
        private Int32 _id;
        /// <summary></summary>
        public virtual Int32  id
        { 
            get { return _id; }
            set { _id = value; }
        }
        private String _name;
        /// <summary></summary>
        public virtual String  name
        { 
            get { return _name; }
            set { _name = value; }
        }
        private Int32 _mark_id;
        /// <summary></summary>
        public virtual Int32  mark_id
        { 
            get { return _mark_id; }
            set { _mark_id = value; }
        }
        private DateTime _create_time;
        /// <summary></summary>
        public virtual DateTime  create_time
        { 
            get { return _create_time; }
            set { _create_time = value; }
        }
        private DateTime _update_time;
        /// <summary></summary>
        public virtual DateTime  update_time
        { 
            get { return _update_time; }
            set { _update_time = value; }
        }
        #region 获取/设置 字段值
        /// <summary>
        /// 获取/设置 字段值。
        /// 一个索引，基类使用反射实现。
        /// 派生实体类可重写该索引，以避免反射带来的性能损耗
        /// </summary>
        /// <param name="name">字段名</param>
        /// <returns></returns>
        public override  Object this[String name]
        {
            get
            {
                switch (name)
                {
                     case __.id : return _id;
                     case __.name : return _name;
                     case __.mark_id : return _mark_id;
                     case __.create_time : return _create_time;
                     case __.update_time : return _update_time;
                    default: return null;
                }
            }
            set
            {
                switch (name)
                {
                      case __.id: _id = Convert.ToInt32(value); break;
                      case __.name: _name = Convert.ToString(value); break;
                      case __.mark_id: _mark_id = Convert.ToInt32(value); break;
                      case __.create_time: _create_time = Convert.ToDateTime(value); break;
                      case __.update_time: _update_time = Convert.ToDateTime(value); break;
                    default: break;
                }
            }
           
        }
        #endregion
        #region 字段名
        private static _Fields _fields;
        public virtual Isys_role_ _X
        {
             get { if (_fields == null) { _fields = new _Fields(); } return _fields; }
        }
        public static Isys_role_ _
        {
             get { if (_fields == null) { _fields = new _Fields(); } return _fields; }
        }
        ///<summary>取得sys_role字段信息的快捷方式</summary>
        private  partial class _Fields:Isys_role_
        {
            private Field _id=FindByName(__.id);
            /// <summary></summary>
            public Field id {get{return _id;}}
            private Field _name=FindByName(__.name);
            /// <summary></summary>
            public Field name {get{return _name;}}
            private Field _mark_id=FindByName(__.mark_id);
            /// <summary></summary>
            public Field mark_id {get{return _mark_id;}}
            private Field _create_time=FindByName(__.create_time);
            /// <summary></summary>
            public Field create_time {get{return _create_time;}}
            private Field _update_time=FindByName(__.update_time);
            /// <summary></summary>
            public Field update_time {get{return _update_time;}}
            static  Field FindByName(String name) { return FindFieldByName(name); }
        }
        /// <summary>取得sys_role字段名称的快捷方式</summary>
        partial class __
        {
            /// <summary></summary>
            public const String id="id";
            /// <summary></summary>
            public const String name="name";
            /// <summary></summary>
            public const String mark_id="mark_id";
            /// <summary></summary>
            public const String create_time="create_time";
            /// <summary></summary>
            public const String update_time="update_time";

        }
        #endregion
    }

 
 }