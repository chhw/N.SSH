﻿using Domain.IDomain;
using Domain.IDomain.IDomian;
using PX.Code.Base;
using PX.Code.Base.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain
{
    [Serializable]
    [EntityAttribute(ConnName="Con",Name="")]
    public partial class sys_user: Entity<sys_user>,Isys_user
    {
        private Int32 _id;
        /// <summary></summary>
        public virtual Int32  id
        { 
            get { return _id; }
            set { _id = value; }
        }
        private String _name;
        /// <summary></summary>
        public virtual String  name
        { 
            get { return _name; }
            set { _name = value; }
        }
        private String _login_name;
        /// <summary></summary>
        public virtual String  login_name
        { 
            get { return _login_name; }
            set { _login_name = value; }
        }
        private String _pwd;
        /// <summary></summary>
        public virtual String  pwd
        { 
            get { return _pwd; }
            set { _pwd = value; }
        }
        private String _email;
        /// <summary></summary>
        public virtual String  email
        { 
            get { return _email; }
            set { _email = value; }
        }
        private DateTime _create_time;
        /// <summary></summary>
        public virtual DateTime  create_time
        { 
            get { return _create_time; }
            set { _create_time = value; }
        }
        private DateTime _update_time;
        /// <summary></summary>
        public virtual DateTime  update_time
        { 
            get { return _update_time; }
            set { _update_time = value; }
        }
        #region 获取/设置 字段值
        /// <summary>
        /// 获取/设置 字段值。
        /// 一个索引，基类使用反射实现。
        /// 派生实体类可重写该索引，以避免反射带来的性能损耗
        /// </summary>
        /// <param name="name">字段名</param>
        /// <returns></returns>
        public override  Object this[String name]
        {
            get
            {
                switch (name)
                {
                     case __.id : return _id;
                     case __.name : return _name;
                     case __.login_name : return _login_name;
                     case __.pwd : return _pwd;
                     case __.email : return _email;
                     case __.create_time : return _create_time;
                     case __.update_time : return _update_time;
                    default: return null;
                }
            }
            set
            {
                switch (name)
                {
                      case __.id: _id = Convert.ToInt32(value); break;
                      case __.name: _name = Convert.ToString(value); break;
                      case __.login_name: _login_name = Convert.ToString(value); break;
                      case __.pwd: _pwd = Convert.ToString(value); break;
                      case __.email: _email = Convert.ToString(value); break;
                      case __.create_time: _create_time = Convert.ToDateTime(value); break;
                      case __.update_time: _update_time = Convert.ToDateTime(value); break;
                    default: break;
                }
            }
           
        }
        #endregion
        #region 字段名
        private static _Fields _fields;
        public virtual Isys_user_ _X
        {
             get { if (_fields == null) { _fields = new _Fields(); } return _fields; }
        }
        public static Isys_user_ _
        {
             get { if (_fields == null) { _fields = new _Fields(); } return _fields; }
        }
        ///<summary>取得sys_user字段信息的快捷方式</summary>
        private  partial class _Fields:Isys_user_
        {
            private Field _id=FindByName(__.id);
            /// <summary></summary>
            public Field id {get{return _id;}}
            private Field _name=FindByName(__.name);
            /// <summary></summary>
            public Field name {get{return _name;}}
            private Field _login_name=FindByName(__.login_name);
            /// <summary></summary>
            public Field login_name {get{return _login_name;}}
            private Field _pwd=FindByName(__.pwd);
            /// <summary></summary>
            public Field pwd {get{return _pwd;}}
            private Field _email=FindByName(__.email);
            /// <summary></summary>
            public Field email {get{return _email;}}
            private Field _create_time=FindByName(__.create_time);
            /// <summary></summary>
            public Field create_time {get{return _create_time;}}
            private Field _update_time=FindByName(__.update_time);
            /// <summary></summary>
            public Field update_time {get{return _update_time;}}
            static  Field FindByName(String name) { return FindFieldByName(name); }
        }
        /// <summary>取得sys_user字段名称的快捷方式</summary>
        partial class __
        {
            /// <summary></summary>
            public const String id="id";
            /// <summary></summary>
            public const String name="name";
            /// <summary></summary>
            public const String login_name="login_name";
            /// <summary></summary>
            public const String pwd="pwd";
            /// <summary></summary>
            public const String email="email";
            /// <summary></summary>
            public const String create_time="create_time";
            /// <summary></summary>
            public const String update_time="update_time";

        }
        #endregion
    }

 
 }