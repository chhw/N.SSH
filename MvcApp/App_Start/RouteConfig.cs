﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using MvcApp.Controllers;
using MvcApp.Controllers.Admin;
using MvcApp.Common;
namespace MvcApp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{resource}.aspx/{*pathInfo}");
            routes.IgnoreRoute("{resource}.rdlc/{*pathInfo}");
            routes.IgnoreRoute("{resource}.ascx/{*pathInfo}");

            routes.MapRoute(
                "AdminIndex",
                "Admin/Index",
                new { controller = "Index", action = "Index" },
                new[] { "MvcApp.Controllers.Admin" }
           );

            routes.MapRoute(
                "AdminUser",
                "Admin/{controller}/{action}/{id}",
                new { controller = "User", action = "Index", id = UrlParameter.Optional },
                new[] { "MvcApp.Controllers.Admin" }
           );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Default", action = "Index", id = UrlParameter.Optional }
            );
         

            //Note: Please don't change the sort of the routes
            routes.Add("Route One Para", new CustomRoute(
                "{controller}/{action}/{id}",
                new RouteValueDictionary(), // defaults
                new RouteValueDictionary(), // data tokens
                new RouteValueDictionary(), // constaints
                new MvcRouteHandler()));

        }
    }
}