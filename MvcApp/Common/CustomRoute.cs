﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using NewLife.Extension;

namespace MvcApp.Common
{
    public class CustomRoute: RouteBase
    {
        /// <summary>
        /// Gets the inner route.
        /// </summary>
        /// <value>The inner route.</value>
        public Route InnerRoute { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomRoute"/> class.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <param name="defaults">The defaults.</param>
        /// <param name="constaints">The constaints.</param>
        /// <param name="dataTokens">The data tokens.</param>
        /// <param name="routeHandler">The route handler.</param>
        public CustomRoute(
            string url,
            RouteValueDictionary defaults,
            RouteValueDictionary constaints,
            RouteValueDictionary dataTokens,
            IRouteHandler routeHandler)
        {
            this.InnerRoute = new Route(
                url,
                defaults,
                constaints,
                dataTokens,
                routeHandler);
        }


        /// <summary>
        /// When overridden in a derived class, returns route information about the request.
        /// </summary>
        /// <param name="httpContext">An object that encapsulates information about the HTTP request.</param>
        /// <returns>An object that contains the values from the route definition if the route matches the current request, or null if the route does not match the request.</returns>
        public override RouteData GetRouteData(HttpContextBase httpContext)
        {
            var routeData = this.InnerRoute.GetRouteData(httpContext);

            if (routeData == null) return null;

            var modifiedRouteData = new RouteData(this.InnerRoute, new MvcRouteHandler());

            //check current route whether match the Defaults Definition of the InnerRoute
            if (this.InnerRoute.Defaults.Count > 0)
            {
                bool isMatchDefaults = false;

                foreach (var rValue in routeData.Values)
                {
                    if (this.InnerRoute.Defaults.Keys.Contains(rValue.Key)
                        && this.InnerRoute.Defaults[rValue.Key] != null && this.InnerRoute.Defaults[rValue.Key] != UrlParameter.Optional
                        && rValue.Value != null && this.InnerRoute.Defaults[rValue.Key].ToString().Equals(rValue.Value.ToString()))
                    {
                        isMatchDefaults = true;
                        break;
                    }

                }

                if (!isMatchDefaults)
                    return null;
            }

            foreach (var pair in routeData.Values)
            {
                if (pair.Key.Equals("controller")
                    || pair.Key.Equals("action"))
                {
                    modifiedRouteData.Values.Add(pair.Key, pair.Value);
                }
                else
                {
                    string pairValue = string.Empty;

                    if (pair.Value != null)
                    {
                        pairValue = pair.Value.ToString();

                        //If the pairValue is a integer, do not need convert from base64 string,
                        //this is in order to be compatible with integer routing parameters
                        int result;
                        if (!int.TryParse(pairValue,out result))
                        {
                            try
                            {
                                pairValue = pair.Value.ToString().ToBase64().ToStr();
                            }
                            catch
                            { }
                        }
                    }

                    modifiedRouteData.Values.Add(pair.Key, pairValue);
                }
            }

            return modifiedRouteData;
        }

        /// <summary>
        /// When overridden in a derived class, checks whether the route matches the specified values, and if so, generates a URL and retrieves information about the route.
        /// </summary>
        /// <param name="requestContext">An object that encapsulates information about the requested route.</param>
        /// <param name="values">An object that contains the parameters for a route.</param>
        /// <returns>An object that contains the generated URL and information about the route, or null if the route does not match <paramref name="values" />.</returns>
        public override VirtualPathData GetVirtualPath(RequestContext requestContext, RouteValueDictionary values)
        {
            var routeValues = new RouteValueDictionary();

            //check current route whether match the Defaults Definition of the InnerRoute
            if (this.InnerRoute.Defaults.Count > 0)
            {
                bool isMatchDefaults = false;

                foreach (var rValue in values)
                {
                    if (this.InnerRoute.Defaults.Keys.Contains(rValue.Key)
                        && this.InnerRoute.Defaults[rValue.Key] != null && this.InnerRoute.Defaults[rValue.Key] != UrlParameter.Optional
                        && rValue.Value != null && this.InnerRoute.Defaults[rValue.Key].ToString().Equals(rValue.Value.ToString()))
                    {
                        isMatchDefaults = true;
                        break;
                    }

                }

                if (!isMatchDefaults)
                    return null;
            }

            foreach (var pair in values)
            {
                if (pair.Key.Equals("controller")
                    || pair.Key.Equals("action"))
                {
                    routeValues.Add(pair.Key, pair.Value);
                }
                else
                {
                    string pairValue = string.Empty;
                    if (pair.Value != null)
                        pairValue = pair.Value.ToString().ToBase64().ToStr();

                    routeValues.Add(pair.Key, pairValue);
                }
            }

            return this.InnerRoute.GetVirtualPath(requestContext, routeValues);

        }
    }
}