﻿using NewLife.Configuration;
using PX.Code.Base;
using PX.Code.Base.Common;
using PX.Code.Base.Entity;
using PX.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace PX.Code.Core.EntityDAO
{
    /// <summary>数据实体类基类。所有数据实体类都必须继承该类。</summary>
    public partial class EntityDAO<TEntity> : EntityDAOBase<TEntity> where TEntity :class, IEntity, new()
    {
        public class _
        {
            public static void BeginTrans()
            {
                entitySession.BeginTrans();
            }
            public static void Commit()
            {
                entitySession.Commit();
            }
            public static void Rollback()
            {
                entitySession.Rollback();
            }
            /// <summary>
            /// 插入数据
            /// </summary>
            /// <returns></returns>
            public static Int32 Insert(TEntity t)
            {
                return (Int32)entitySession.Insert(t);
            }

            /// <summary>更新数据库</summary>
            /// <returns></returns>
            public static Boolean Update(TEntity t)
            {
                return entitySession.Update(t);
            }

            /// <summary>从数据库中删除该对象</summary>
            /// <returns></returns>
            public static Boolean Delete(TEntity t)
            {
                return entitySession.Delete(t);
            }

            /// <summary>检查这个对象是否已经存在Session中。如果对象不在，调用Save(object)来保存。如果对象存在，检查这个对象是否改变了。如果对象改变，调用Update(object)来更新。</summary>
            /// <returns></returns>
            public static Boolean Save(TEntity t)
            {
                return entitySession.Save(t);
            }

            /// <summary>从数据库中删除该对象</summary>
            /// <returns></returns>
            public static Int32 DeleteAll(AbstractExpressions icriterion)
            {
                return entitySession.DeleteAll(icriterion);
            }
            public static IEntityList<TEntity> FindAll()
            {
                return entitySession.FindAll();
            }
            public static IEntityList<TEntity> FindAll(String strWhere)
            {
                return entitySession.FindAll(strWhere, string.Empty);
            }
            public static IEntityList<TEntity> FindAll(Field field, object obj)
            {
                return entitySession.FindAll(field, obj);
            }
            public static IEntityList<TEntity> FindAll(Field[] fields, object[] obj)
            {
                return entitySession.FindAll(fields, obj);
            }
            public static IEntityList<TEntity> FindAll(IList<Field> fields, object[] obj)
            {
                return entitySession.FindAll(fields, obj);
            }
            public static IEntityList<TEntity> FindAll(String field, object obj)
            {
                Field fieldtemp = entitySession.TableItem.FindByName(field);
                return entitySession.FindAll(fieldtemp, obj);
            }
            public static IEntityList<TEntity> FindAll(String[] fields, object[] obj)
            {
                List<Field> list = new List<Field>();
                for (int i = 0; i < fields.Length; i++)
                {
                    list.Add(entitySession.TableItem.FindByName(fields[i]));
                }
                return entitySession.FindAll(list, obj);
            }
            public static IEntityList<TEntity> FindAll(AbstractExpressions icriterion, PxOrder order)
            {
                return entitySession.FindAll(icriterion, order, 0, int.MaxValue);
            }
            public static IEntityList<TEntity> FindAll(AbstractExpressions icriterion)
            {
                return entitySession.FindAll(icriterion, null, 0, int.MaxValue);
            }
            public static IEntityList<TEntity> FindAll(AbstractExpressions icriterion, PxOrder order, Int32 startRowIndex, Int32 maximumRows, out Int32 rowCount)
            {
                rowCount = entitySession.FindCount(icriterion);
                return entitySession.FindAll(icriterion, order, startRowIndex, maximumRows);
            }
            public static IEntityList<TEntity> FindAll(String icriterion, PxOrder order, Int32 startRowIndex, Int32 maximumRows, out Int32 rowCount)
            {
                rowCount = entitySession.FindCount(icriterion);
                return entitySession.FindAll(icriterion, order.ToString(), startRowIndex, maximumRows);
            }
            public static TEntity Find(Field field, object obj)
            {
                return entitySession.Find(field, obj);
            }
            public static TEntity Find(Field[] fields, object[] obj)
            {
                return entitySession.Find(fields, obj);
            }
            public static TEntity Find(IList<Field> fields, object[] obj)
            {
                return entitySession.Find(fields, obj);
            }
            public static TEntity Find(String field, object obj)
            {
                Field fieldtemp = entitySession.TableItem.FindByName(field);
                return entitySession.Find(fieldtemp, obj);
            }
            public static TEntity Find(String[] fields, object[] obj)
            {
                List<Field> list = new List<Field>();
                for (int i = 0; i < fields.Length; i++)
                {
                    list.Add(entitySession.TableItem.FindByName(fields[i]));
                }
                return entitySession.Find(list, obj);
            }
            public static TEntity Find(AbstractExpressions icriterion)
            {
                return entitySession.Find(icriterion);
            }
            public static TEntity FindByID(object id)
            {
                return entitySession.FindByID(id);
            }
            public static TEntity LoadData(DataRow dr)
            {
                return dr.PxToEntity<TEntity>();
            }
        }
    }
}
