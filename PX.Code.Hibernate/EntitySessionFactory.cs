﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NewLife.Configuration;
using FluentNHibernate;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;

namespace PX.Code.Hibernate
{
    internal class EntitySessionFactory
    {
        private static Dictionary<string, ISessionFactory> _EntitySessionFactory = null;
        private static Dictionary<string, NHibernate.Cfg.Configuration> _Configuration = null;

        public static Dictionary<string, NHibernate.Cfg.Configuration> getConfigurationDic
        {
            get { return EntitySessionFactory._Configuration; }
        }
        private static readonly object lockHelper = new object();
        public static Dictionary<string, ISessionFactory> getEntitySessionFactoryDic
        {
            get { return EntitySessionFactory._EntitySessionFactory; }
        }

        private EntitySessionFactory()
        {
            _EntitySessionFactory = new Dictionary<string, ISessionFactory>();
            _Configuration = new Dictionary<string, NHibernate.Cfg.Configuration>();
        }
        public static ISessionFactory getEntitySessionFactory(string strConName)
        {

            if (_EntitySessionFactory == null)
            {
                lock (lockHelper)
                {
                    if (_EntitySessionFactory == null || _Configuration==null)
                    {
                        new EntitySessionFactory();
                    }
                }
            }

            if (!_EntitySessionFactory.ContainsKey(strConName))
            {
                lock (lockHelper)
                {
                    if (!_EntitySessionFactory.ContainsKey(strConName))
                    {
                        string path = string.Empty;
                        if (System.Environment.CurrentDirectory == System.AppDomain.CurrentDomain.BaseDirectory)//Windows应用程序则相等
                        {
                            path = AppDomain.CurrentDomain.BaseDirectory;
                        }
                        else
                        {
                            path = AppDomain.CurrentDomain.BaseDirectory + "Bin";
                        }
                        string strPath = System.IO.Path.Combine(path, Config.AppSettings[strConName]);
                       
                        NHibernate.Cfg.Configuration configuration= new NHibernate.Cfg.Configuration().Configure(strPath);
                        //从Nhibernate创建可用的Fluent配置
                        //FluentConfiguration fconfiguration = Fluently.Configure(configuration);

                        //MappingConfiguration mp = new MappingConfiguration();
                        ////configuration.ma.GetProperty();
                        //mp.FluentMappings.AddFromAssembly(System.Reflection.Assembly.Load("DAO.Hibernate"));
                        //mp.FluentMappings.ExportTo(path+"_xmlmapping_temp");


                          //  m => m.FluentMappings
                          //.AddFromAssembly(System.Reflection.Assembly.Load("Domain"))
                          //.ExportTo(path)
                        //映射程序集
                        FluentConfiguration fc = Fluently.Configure(configuration).Database(
                            MsSqlConfiguration.MsSql2012.ConnectionString("server=.;database=N_SSH;uid=sa;pwd=123456;")
                            )
                          .Mappings(m => m.FluentMappings
                          .AddFromAssembly(System.Reflection.Assembly.Load("DAO.Hibernate"))
                          .ExportTo(path + "\\xmlmapping_temp"));
              
                        configuration = fc.BuildConfiguration();
                       
                        _Configuration.Add(strConName, configuration);



                        _EntitySessionFactory.Add(strConName, configuration.BuildSessionFactory());
                    }

                }

            }
            return _EntitySessionFactory[strConName];
        }
        public static NHibernate.Cfg.Configuration getEntityConfiguration(string strConName)
        {
            if (!_Configuration.ContainsKey(strConName))
            {
                getEntitySessionFactory(strConName);
            }
            return _Configuration[strConName];
        }
    }
}
