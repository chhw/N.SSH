﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Hosting;
using NHibernate;
using NHibernate.Context;
using PX.Code.Log;

namespace PX.Code.Hibernate
{
    internal class EntitySessionModule : IHttpModule
    {
        private const string SessionFactoryMapKey = "NHibernate.Context.WebSessionContext.SessionFactoryMapKey";
        private ISessionFactory SessionFactory { get; set; }

        public EntitySessionModule(ISessionFactory SessionFactory)
        {
            this.SessionFactory = SessionFactory;
        }
        public void Init(HttpApplication context)
        {
            //context.BeginRequest += new EventHandler(Application_BeginRequest);
            //context.EndRequest += new EventHandler(Application_EndRequest);
        }

        public void Dispose()
        {
        }
        public void LessRequest()
        {
            if (HttpContext.Current==null)
            {
                //模拟web请求用于单元测试
                Thread.GetDomain().SetData(".appPath", "c:\\inetpub\\wwwroot\\webapp\\");
                Thread.GetDomain().SetData(".appVPath", "/");
                TextWriter tw = new StringWriter();
                HttpWorkerRequest wr = new SimpleWorkerRequest
                ("WebForm1.aspx", "friendId=1300000000", tw);
                HttpContext.Current = new HttpContext(wr);
            }
            var ht = HttpContext.Current.Items[SessionFactoryMapKey] as System.Collections.Hashtable;
            ISession currentSession = null;
            if (ht != null) { currentSession = ht[SessionFactory] as ISession; }
            if (currentSession == null)
            {
                currentSession = SessionFactory.OpenSession(new SQLWatcher());
                WebSessionContext.Bind(currentSession);//还是用NHibernate自带的方法将Session绑定到HttpContext.Current上.
            }
        }
    }
}
