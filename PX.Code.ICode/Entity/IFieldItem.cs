﻿using System;
namespace PX.Code.Base.Entity
{
    public interface IFieldItem
    {
        PX.Core.AbstractExpressions And(PX.Core.AbstractExpressions ihs1, PX.Core.AbstractExpressions ihs2);
        PX.Core.AbstractExpressions Between(object obj1, object obj2);
        PX.Core.AbstractExpressions Contains(string str);
        PX.Core.AbstractExpressions Eq(object obj);
        PX.Core.AbstractExpressions EqProperty(string str);
        PX.Core.AbstractExpressions Ge(object obj);
        PX.Core.AbstractExpressions Gt(object obj);
        PX.Core.AbstractExpressions In(System.Collections.ICollection objs);
        PX.Core.AbstractExpressions In(object[] objs);
        PX.Core.AbstractExpressions IsEmpty();
        PX.Core.AbstractExpressions IsNotEmpty();
        PX.Core.AbstractExpressions IsNotNull();
        PX.Core.AbstractExpressions IsNull();
        PX.Core.AbstractExpressions Le(object obj);
        PX.Core.AbstractExpressions Like(string str);
        PX.Core.AbstractExpressions LikeAnyWhere(string str);
        PX.Core.AbstractExpressions LikeEnd(string str);
        PX.Core.AbstractExpressions LikeStart(string str);
        PX.Core.AbstractExpressions Lt(object obj);
        string Name { get; set; }
        PX.Core.AbstractExpressions NotEq(object obj);
        PX.Core.AbstractExpressions Or(PX.Core.AbstractExpressions ihs1, PX.Core.AbstractExpressions ihs2);
        PX.Core.PxOrder OrderAsc();
        PX.Core.PxOrder OrderDesc();
        object ToType(object obj);
        Type Type { get; }
    }
}
