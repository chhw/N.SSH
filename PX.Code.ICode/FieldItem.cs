﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Linq;
using PX.Core;
using PX.Code.Base.Entity;
using PX.Code.Base.Common;

namespace PX.Code.Base
{
    /// <summary>数据属性元数据以及特性</summary>
    public partial class FieldItem :IFieldItem 
    {

        #region 属性
        //private Column _Column = null;

        //internal Column Column
        //{
        //    get { return _Column; }
        //    set { _Column = value; }
        //}

        private String _Name;

        public String Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        private Type _type;

        public Type Type
        {
            get { return _type; }
            set { _type = value; }
        }
        //public Type Type { get { return Column.Value.Type.ReturnedClass; } }
        #endregion
        #region Expressions
        /// <summary>
        /// =
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public AbstractExpressions Eq(object obj)
        {

            obj = ToType(obj);
            return RestrictionsFactory.Eq(Name, obj);
        }
        public AbstractExpressions EqProperty(String str)
        {
            return RestrictionsFactory.EqProperty(Name, str);
        }

        public AbstractExpressions NotEq(object obj)
        {
            obj = ToType(obj);
            return RestrictionsFactory.NotEq(Name, obj);
        }
        ///// <summary>
        ///// 利用Map来进行多个等于的限制
        ///// </summary>
        ///// <param name="dicValue"></param>
        ///// <returns></returns>
        //public AbstractExpressions AllEq(IDictionary dicValue)
        //{
        //    return RestrictionsFactory.AllEq(dicValue);
        //}
        /// <summary>
        /// >
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public AbstractExpressions Gt(object obj)
        {
            obj = ToType(obj);
            return RestrictionsFactory.Gt(Name, obj);
        }
        /// <summary>
        /// >=
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public AbstractExpressions Ge(object obj)
        {
            obj = ToType(obj);
            return RestrictionsFactory.Ge(Name, obj);
        }
        /// <summary>
        ///  <
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public AbstractExpressions Lt(object obj)
        {
            obj = ToType(obj);
            return RestrictionsFactory.Lt(Name, obj);
        }

        /// <summary>
        /// <=
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public AbstractExpressions Le(object obj)
        {
            obj = ToType(obj);
            return RestrictionsFactory.Le(Name, obj);
        }
        public AbstractExpressions Between(object obj1, object obj2)
        {
            obj1 = ToType(obj1);
            obj2 = ToType(obj2);
            return RestrictionsFactory.Between(Name, obj1, obj2);
        }
        /// <summary>
        /// in
        /// </summary>
        /// <param name="objs"></param>
        /// <returns></returns>
        public AbstractExpressions In(object[] objs)
        {
            for (int i = 0; i < objs.Length; i++)
            {
                objs[i] = ToType(objs[i]);
            }
            return RestrictionsFactory.In(Name, objs);
        }
        /// <summary>
        /// in
        /// </summary>
        /// <param name="objs"></param>
        /// <returns></returns>
        public AbstractExpressions In(ICollection objs)
        {
            return RestrictionsFactory.In(Name, objs);
        }
        public AbstractExpressions Contains(String str)
        {
            return LikeAnyWhere(str);
        }
        public AbstractExpressions Like(String str)
        {
            return RestrictionsFactory.LikeAnyWhere(Name, str);
        }
        public AbstractExpressions LikeAnyWhere(String str)
        {
            return RestrictionsFactory.LikeAnyWhere(Name, str);
        }
        public AbstractExpressions LikeStart(String str)
        {
            return RestrictionsFactory.LikeStart(Name, str);
        }
        public AbstractExpressions LikeEnd(String str)
        {
            return RestrictionsFactory.LikeEnd(Name, str);
        }

        /// <summary>
        /// And
        /// </summary>
        /// <param name="ihs1"></param>
        /// <param name="ihs2"></param>
        /// <returns></returns>
        public AbstractExpressions And(AbstractExpressions ihs1, AbstractExpressions ihs2)
        {
            return RestrictionsFactory.And(ihs1, ihs2);
        }
        /// <summary>
        /// or
        /// </summary>
        /// <param name="ihs1"></param>
        /// <param name="ihs2"></param>
        /// <returns></returns>
        public AbstractExpressions Or(AbstractExpressions ihs1, AbstractExpressions ihs2)
        {
            return RestrictionsFactory.Or(ihs1, ihs2);
        }
        public AbstractExpressions IsNull()
        {
            return RestrictionsFactory.IsNull(Name);
        }
        public AbstractExpressions IsNotNull()
        {
            return RestrictionsFactory.IsNotNull(Name);
        }
        public AbstractExpressions IsEmpty()
        {
            return RestrictionsFactory.IsEmpty(Name);
        }
        public AbstractExpressions IsNotEmpty()
        {
            return RestrictionsFactory.IsNotEmpty(Name);
        }
        public PxOrder OrderAsc()
        {
            return PxOrder.Asc(Name);
        }
        public PxOrder OrderDesc()
        {
            return PxOrder.Desc(Name);
        }
        public object ToType(object obj)
        {
            if (!Type.Equals(obj.GetType()))
            {
                obj = Convert.ChangeType(obj, Type);
            }
            return obj;
        }

        #endregion
        #region 构造
        public FieldItem()
        { 
        
        }
        #endregion
        #region 重载运算符
        /// <summary>大于</summary>
        /// <param name="field">字段</param>
        /// <param name="value">数值</param>
        /// <returns></returns>
        public static AbstractExpressions operator >(FieldItem field, Object value) { return field.Gt(value); }

        /// <summary>小于</summary>
        /// <param name="field">字段</param>
        /// <param name="value">数值</param>
        /// <returns></returns>
        public static AbstractExpressions operator <(FieldItem field, Object value) { return field.Lt(value); }

        /// <summary>大于等于</summary>
        /// <param name="field">字段</param>
        /// <param name="value">数值</param>
        /// <returns></returns>
        public static AbstractExpressions operator >=(FieldItem field, Object value) { return field.Ge(value); }

        /// <summary>小于等于</summary>
        /// <param name="field">字段</param>
        /// <param name="value">数值</param>
        /// <returns></returns>
        public static AbstractExpressions operator <=(FieldItem field, Object value) { return field.Le(value); }
        #endregion
    }

    public class Field : FieldItem
    {
        public static Field CreateFieldItem(String columnName,Type type)
        {
            Field fi = new Field();
            fi.Name = columnName;
            fi.Type = type;
            return fi;
        }
        /// <summary>等于</summary>
        /// <param name="field">字段</param>
        /// <param name="value">数值</param>
        /// <returns></returns>
        public static AbstractExpressions operator ==(Field field, Object value) { return field.Eq(value); }

        /// <summary>不等于</summary>
        /// <param name="field">字段</param>
        /// <param name="value">数值</param>
        /// <returns></returns>
        public static AbstractExpressions operator !=(Field field, Object value) { return field.NotEq(value); }

        /// <summary>重写一下</summary>
        /// <returns></returns>
        public override int GetHashCode() { return base.GetHashCode(); }

        /// <summary>重写一下</summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj) { return base.Equals(obj); }


        public override string ToString()
        {
            return Name;
        }
        /// <summary>类型转换</summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static implicit operator String(Field obj)
        {
            return !obj.Equals(null) ? obj.Name : null;
        }
    }
}