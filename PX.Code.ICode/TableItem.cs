﻿using PX.Code.Base.Entity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Xml.Serialization;

namespace PX.Code.Base
{
    /// <summary>数据表元数据</summary>
    public class TableItem : ITableItem
    {
        #region 特性
        private Type _EntityType;
        /// <summary>实体类型</summary>
        public Type EntityType { get { return _EntityType; } set { _EntityType = value; } }

        private String _Name=null;

        public String Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        private Field _unique;

        public Field Unique
        {
            get { return _unique; }
            set { _unique = value; }
        }
        /// <summary>唯一键，返回第一个标识列或者唯一的键</summary>
        
      
        #endregion

        #region 属性
   
        #endregion

        #region 扩展属性
        private List<Field> _Fields;
        /// <summary>数据字段</summary>
        [Description("数据字段")]
        public List<Field> Fields { get { return _Fields; } set { _Fields = value; } }

        private List<String> _FieldNames;

        public List<String> FieldNames { get { return _FieldNames; } set { _FieldNames = value; } }
        #endregion

        #region 构造

        public TableItem():base()
        {
           
        }
     
        public static ITableItem CreateTableItem(List<Field> list,Type type)
        {
            TableItem tableItem=new TableItem();
            tableItem.EntityType = type;
            tableItem.Fields = list;
            tableItem.FieldNames = new List<String>();
            foreach (var item in list)
            {
                tableItem.FieldNames.Add(item.Name);
            }
            return tableItem;
        }
  
        #endregion

        #region 方法
        /// <summary>根据名称查找</summary>
        /// <param name="name">名称</param>
        /// <returns></returns>
        public Field FindByName(String name)
        {
            if (String.IsNullOrEmpty(name)) throw new ArgumentNullException("name");

            foreach (var item in Fields)
            {
                if (item.Name.Equals(name)) return item;
            }
            return null;
        }

        #endregion

    }
}