using System;
using System.Collections.Generic;
using System.Text;


namespace PX.Criterion
{
	[Serializable]
	public abstract class AbstractEmptinessExpression : AbstractCriterion
	{

		private readonly string propertyName;

		protected abstract bool ExcludeEmpty { get; }


		protected AbstractEmptinessExpression(string propertyName)
		{
			this.propertyName = propertyName;
		}

		

		public override sealed string ToString()
		{
			return propertyName + (ExcludeEmpty ? " is not empty" : " is empty");
		}

	
	}
}
