﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PX.Core
{
    /// <summary>
    /// 用于扩展条件合并，方便实现链式编程
    /// </summary>
    public static class ExpressionsExtend
    {
        public static AbstractExpressions And(this AbstractExpressions lhs, AbstractExpressions rhs)
        {
            return new AndExpressions(lhs, rhs);
        }
        public static AbstractExpressions Or(this AbstractExpressions lhs, AbstractExpressions rhs)
        {
            return new OrExpressions(lhs, rhs);
        }
    }
}
