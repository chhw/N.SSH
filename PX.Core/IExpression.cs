﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PX.Core
{
    public interface IExpression
    {
        string ColumnName { get; set; }
        string ColumnName2 { get; set; }
        ComparisonWhere ComparisonWhere { get; set; }
        object ParamEndValue { get; set; }
        object ParamStartValue { get; set; }
        object[] ParamValues { get; set; }
    }
}
