using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


using PX.Util;
namespace PX.Criterion
{
	/// <summary>
	/// An <see cref="ICriterion"/> that constrains the property
	/// to a specified list of values.
	/// </summary>
	/// <remarks>
	/// InExpression - should only be used with a Single Value column - no multicolumn properties...
	/// </remarks>
	[Serializable]
	public class InExpression : AbstractCriterion
	{
		private readonly IProjection _projection;
		private readonly string _propertyName;
		private object[] _values;

		/// <summary>
		/// Initializes a new instance of the <see cref="InExpression"/> class.
		/// </summary>
		/// <param name="projection">The projection.</param>
		/// <param name="values">The _values.</param>
		public InExpression(IProjection projection, object[] values)
		{
			_projection = projection;
			_values = values;
		}

		public InExpression(string propertyName, object[] values)
		{
			_propertyName = propertyName;
			_values = values;
		}

		public object[] Values
		{
			get { return _values; }
			protected set { _values = value; }
		}

		public override string ToString()
		{
			return (_projection ?? (object)_propertyName) + " in (" + StringHelper.ToString(_values) + ')';
		}
	}
}
