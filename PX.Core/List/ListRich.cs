﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PX.Core.List
{
    public  class ListRich<T>:List<T>
    {
          /// <summary>构造一个实体对象集合</summary>
        public ListRich() { }

        /// <summary>构造一个实体对象集合</summary>
        /// <param name="collection"></param>
        public ListRich(IEnumerable<T> collection) : base(collection) { }

        /// <summary>构造一个实体对象集合</summary>
        /// <param name="capacity"></param>
        public ListRich(Int32 capacity) : base(capacity) { }

        /// <summary>初始化</summary>
        /// <param name="collection"></param>
        public ListRich(IEnumerable collection)
        {
            if (collection != null)
            {
                foreach (T item in collection)
                {
                    Add(item);
                }
            }
        }

        /// <summary>任意集合转为实体集合</summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        public static ListRich<T> From(IEnumerable collection) { return new ListRich<T>(collection); }

        public static ListRich<T> From(IEnumerable<T> collection) { return new ListRich<T>(collection); }
    }
}
