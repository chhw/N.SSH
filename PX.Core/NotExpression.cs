using System;
using System.Collections.Generic;



namespace PX.Criterion
{
	/// <summary>
	/// An <see cref="ICriterion"/> that negates another <see cref="ICriterion"/>.
	/// </summary>
	[Serializable]
	public class NotExpression : AbstractCriterion
	{
		private readonly ICriterion _criterion;

		/// <summary>
		/// Initialize a new instance of the <see cref="NotExpression" /> class for an
		/// <see cref="ICriterion"/>
		/// </summary>
		/// <param name="criterion">The <see cref="ICriterion"/> to negate.</param>
		public NotExpression(ICriterion criterion)
		{
			_criterion = criterion;
		}



	

		public override string ToString()
		{
			return string.Format("not ({0})", _criterion.ToString());
		}


	}
}