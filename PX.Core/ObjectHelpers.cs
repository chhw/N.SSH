
namespace PX.Util
{
	/// <summary>
	/// Various small helper methods.
	/// </summary>
	public static class ObjectHelpers
	{
		/// <summary>
		/// Return an identifying string representation for the object, taking
		/// PX proxies into account. The returned string will be "null",
		/// "classname@hashcode(hash)", or "entityname#identifier". If the object
		/// is an uninitialized PX proxy, take care not to initialize it.
		/// </summary>
		public static string IdentityToString(object obj)
		{
			if (obj == null)
			{
				return "null";
			}

            //var proxy = obj as Proxy.IPXProxy;

            //if (null != proxy)
            //{
            //    var init = proxy.HibernateLazyInitializer;
            //    return StringHelper.Unqualify(init.EntityName) + "#" + init.Identifier;
            //}

			return StringHelper.Unqualify(obj.GetType().FullName) + "@" + obj.GetHashCode() + "(hash)";
		}
	}
}
