﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PX.Core
{
    public class OrExpressions : LExpressions
    {
        public IOperatorExpression op
        {
            get { return new OperatorExpression() { OperatorWhere = OperatorWhere.Or }; }
        }
        public OrExpressions(AbstractExpressions ie1, AbstractExpressions ie2)
        {
            ExpressionList.AddRange(ie1.ExpressionList);
            OperatorExpressionList.AddRange(ie1.OperatorExpressionList);
            OperatorExpressionList.Add(op);
            OperatorExpressionList.AddRange(ie2.OperatorExpressionList);
            ExpressionList.AddRange(ie2.ExpressionList);
        }
    }
}
